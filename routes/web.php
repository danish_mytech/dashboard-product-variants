<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('admin.auth.login.show-login-form');
})->name('index');


Route::group([ 'prefix' => 'admin','namespace' => 'Admin','as' => 'admin.'], function () {
    Route::group(['as' => 'auth.', 'namespace' => 'Auth'], function () {
        Route::get('/', [LoginController::class, 'showLoginForm'])->name('login.show-login-form');
        Route::post('login',[LoginController::class, 'login'])->name('login.login');
        Route::post('logout',[LoginController::class, 'logout'])->name('login.logout');
        Route::get('logout', [LoginController::class, 'logout'])->name('login.logout');
    });

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('home',[DashboardController::class, 'index'])->name( 'home.index');

        Route::resources([
            'categories' => '\App\Http\Controllers\Admin\CategoryController',
            'attributes' => '\App\Http\Controllers\Admin\AttributeController',
            'attributes.sub-attributes' => '\App\Http\Controllers\Admin\SubAttributesController',
            'orders' => '\App\Http\Controllers\Admin\OrderController',
            'products' => '\App\Http\Controllers\Admin\ProductController',
        ]);

        Route::post('list/categories',[CategoryController::class, 'all'])->name('categories.all');
        Route::post('list/orders',[OrderController::class, 'all'])->name('orders.all');
        Route::post('list/products',[ProductController::class, 'all'])->name('products.all');
    });
});
