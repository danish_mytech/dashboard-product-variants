<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 Route::post('/image-upload', [\App\Http\Controllers\Api\IndexController::class, 'imageUpload'])->name('image.upload');
    Route::post('/image-remove', [\App\Http\Controllers\Api\IndexController::class, 'removeImage'])->name('image.remove');
    Route::post('/product', [\App\Http\Controllers\Api\ProductController::class, 'get'])->name('product.get');
    Route::post('/save-product', [\App\Http\Controllers\Api\ProductController::class, 'save'])->name('product.save');

Route::group([ 'middleware' => 'auth:sanctum'], function () {
   
});
