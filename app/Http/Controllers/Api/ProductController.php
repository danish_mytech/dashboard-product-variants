<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProductController
{
    public function get(Request $request){
        $request->validate([
           'id' => 'required'
        ]);
        try {
            $product = Product::with(['productAttributesIds', 'images', 'productCategoryIds'])->where('id', $request->get('id'))->firstOrFail();
            return response(['msg' => 'Product', 'success' => true, 'data' => $product]);
        }
        catch (\Exception $e){
            return response(['msg' => $e->getMessage(), 'success' => false]);
        }
    }

    public function save(ProductRequest $request){
        \DB::beginTransaction();
       try{
           $id = ($request->filled('id')) ? $request->get('id') : 0;
           $data = $request->only(['title', 'detail', 'price', 'quantity']);
           $product = Product::updateOrCreate(['id' => $id], $data);
           $product->categories()->sync($request->get('categories'));
           if($request->filled('selectedAttributes')){
               $product->attributes()->sync($request->get('selectedAttributes'));
           }
           if($request->filled('images')){
               $productImages = [];
               foreach ($request->get('images') as $image){
                   $productImages[] = new ProductImage([
                       'image' => $image['filename'],
                       'is_default' => $image['isDefault']
                   ]);
               }
               $product->images()->saveMany($productImages);
           }
           \DB::commit();
           return response(['msg' => 'Product Saved', 'success' => true]);
       }catch (\Exception $e){
           \DB::rollback();
           return response(['msg' => $e->getMessage(), 'success' => false]);
       }
    }

}
