<?php


namespace App\Http\Controllers\Api;

use App\Http\Libraries\Uploader;
use Illuminate\Http\Request;

class IndexController
{

    public function imageUpload(Request $request)
    {
        if ($request->file('image')) {
            $uploader = new Uploader('image');
            $image = '';
            if ($uploader->isValidFile()) {
                $uploader->upload('product', $uploader->fileName);
                if ($uploader->isUploaded()) {
                    $image = $uploader->getUploadedPath();
                }
            }
            if (!$uploader->isUploaded()) {
                return response(['msg'=> 'Something went wrong.', 'success' => false]);
            }
            return response(['msg'=> 'Image Upload successfully.', 'success' => true, 'data'=>  ['imageUrl' => url($image), 'image' => $image]]);
        }
        return response(['msg'=> 'Please upload an image.', 'success' => false]);
    }

    public function removeImage(Request $request){
        if (\File::exists($request->image)) {
            unlink($request->image);
            return response(['msg'=> 'Image Deleted successfully.', 'success' => true]);
        }else{
            return response(['msg'=> 'Image cannot be deleted.', 'success' => false]);
        }
    }
}
