<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $user, $breadcrumbs, $breadcrumbTitle;

    public function __construct($userDataKey = 'userData', $guard = null, $getUserCallback = null) {

        $this->middleware(function($request, $next) use($guard, $getUserCallback) {
            $this->user = ($guard=='admin') ? session('ADMIN_DATA'): session('USER_DATA');
            if($getUserCallback){
                $getUserCallback($this->user);
            }
            return $next($request);
        });
        \View::composer('*', function($view) use($userDataKey, $guard) {
            if (!$guard) {
                $view->with([
                    'user' => $this->user,
                    'maintenance_mode' => session('maintenanceMode', 1),
                    'breadcrumbs' => $this->breadcrumbs,
                    'breadcrumbTitle' => $this->breadcrumbTitle,
                ]);
            }
            else {
                $view->with([
                    'user' => $this->user,
                    'maintenance_mode' => session('maintenanceMode', 1),
                    'breadcrumbs' => $this->breadcrumbs,
                    'breadcrumbTitle' => $this->breadcrumbTitle,
                    'currentRouteName' => \Route::current()->getName(),
                ]);
            }
        });
    }
}
