<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Libraries\DataTable;
use App\Http\Repositories\AttributeRepository;
use App\Http\Repositories\CategoryRepository;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\FromValidation;
use App\Models\Category;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class CategoryController extends Controller
{

    public function __construct()
    {
        parent::__construct('adminData', 'admin');
        $this->breadcrumbTitle = 'Categories';
        $this->breadcrumbs[route('admin.home.index')] = ['icon' => 'fa fa-fw fa-home', 'title' => 'Dashboard'];
        $this->breadcrumbs[route('admin.categories.index')] = ['icon' => 'fa fa-fw fa-money', 'title' => 'Manage Categories'];
    }

    public function index()
    {
        return view('admin.categories.index');
    }

    public function all()
    {
        $columns = [
            ['db' => 'id', 'dt' => 'id'],
            ['db' => 'title', 'dt' => 'title'],
            ['db' => 'slug', 'dt' => 'slug'],
            ['db' => 'created_at', 'dt' => 'created_at'],
            ['db' => 'updated_at', 'dt' => 'updated_at'],
        ];
        DataTable::init(new Category(), $columns);
        $category = DataTable::get();
        if (sizeof($category['data']) > 0) {
            foreach ($category['data'] as $key => $data) {
                $category['data'][$key]['actions'] = '
                            <a class="btn btn-light btn-active-light-primary btn-sm " data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                                Actions
                                <span class="svg-icon svg-icon-5 m-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-180.000000) translate(-12.000003, -11.999999)"></path>
                                        </g>
                                    </svg>
                                </span>
                            </a>
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
                                <div class="menu-item px-3">
                                    <a href="' . route('admin.categories.edit', $data['id']) . '" class="menu-link px-3" data-kt-docs-table-filter="edit_row">
                                        Edit
                                    </a>
                                </div>
                                <div class="menu-item px-3">
                                    <a href="javascript:{};" data-url="' . route('admin.categories.destroy', $data['id']) . '" class="menu-link px-3" data-kt-docs-table-filter="delete_row">
                                        Delete
                                    </a>
                                </div>
                            </div>
                        ';
            }
        }
        $category['recordsFiltered'] = $category['meta']['total'];
        $category['recordsTotal'] = $category['meta']['total'];
        return response($category);
    }

    public function getViewParams($id = 0)
    {
        $category = new Category();
        if ($id > 0) {
            $category = Category::findOrFail($id);
        }
        return $category;
    }

    public function edit($id)
    {
        $heading = (($id > 0) ? 'Edit Category' : 'Add Category');
        $this->breadcrumbs['javascript:{};'] = ['icon' => 'fa fa-fw fa-money', 'title' => $heading];
        $category = $this->getViewParams($id);

        return view('admin.categories.edit', [
            'categoryId' => $id,
            'category' => $category,
            'action' => route('admin.categories.update', $id),
        ]);

    }

    public function update(CategoryRequest $request, $id)
    {
        try {
            $data = $request->only(['title']);
            Category::updateOrCreate(['id' => $id],$data);
            return redirect(route('admin.categories.index'))->with('status', 'Operation Successfully.');
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->with('err', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            Category::findOrFail($id)->delete();
            return response(['msg' => 'Category deleted Successfully']);
        }
        catch (\Exception $e) {
            return response(['err' => 'Unable to delete'],400);
        }
    }

}
