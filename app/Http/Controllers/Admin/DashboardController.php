<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct() {
        parent::__construct('adminData', 'admin');

    }

    public function index() {
        $this->breadcrumbTitle = 'Dashboard';
        $this->breadcrumbs['javascript:{};'] = ['icon' => 'fa fa-fw fa-money', 'title' => 'Dashboard '];
        return view('admin.dashboard');
    }
}
