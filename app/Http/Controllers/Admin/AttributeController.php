<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeRequest;
use App\Models\Attribute;

class AttributeController extends Controller
{

    public function __construct()
    {
        parent::__construct('adminData', 'admin');
        $this->breadcrumbTitle = 'Attributes';
        $this->breadcrumbs[route('admin.home.index')] = ['icon' => 'fa fa-fw fa-home', 'title' => 'Dashboard'];

    }

    public function index()
    {
        $this->breadcrumbs['javascript:{};'] = ['icon' => 'fa fa-fw fa-money', 'title' => 'Manage Attributes'];
        $attributes = Attribute::where('parent_id', null)->with(['subAttributes'])->get();
        return view('admin.attributes.index', ['attributes' => $attributes]);
    }

    public function getViewParams($id = 0)
    {
        $attribute = new Attribute();
        if ($id > 0) {
            $attribute = Attribute::findOrFail($id);
        }
        return $attribute;
    }

    public function edit($id)
    {
        $heading = (($id > 0) ? 'Edit Attribute' : 'Add Attribute');
        $this->breadcrumbs[route('admin.attributes.index')] = ['icon' => 'fa fa-fw fa-building', 'title' => 'Manage Attributes'];
        $this->breadcrumbs['javascript:{};'] = ['icon' => 'fa fa-fw fa-money', 'title' => $heading];
        return view('admin.attributes.edit', [
            'method' => 'PUT',
            'attributeId' => $id,
            'attribute' => $this->getViewParams($id),
            'action' => route('admin.attributes.update', $id)
        ]);

    }

    public function update(AttributeRequest $request, $id)
    {
         try {
             $data = $request->only(['title']);
             Attribute::updateOrCreate(['id'=> $id], $data);
             return redirect(route('admin.attributes.index'))->with('status', 'Operation Successfully.');
        } catch (\Exception $e) {
             dd($e->getMessage());
            return redirect()->back()->withInput()->with('err', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            Attribute::findOrFail($id)->delete();
            return response(['msg' => 'Successfully deleted.']);
        } catch (\Exception $e) {
            return response(['err' => $e->getMessage()], 400);
        }
    }


}

