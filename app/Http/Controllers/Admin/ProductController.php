<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Libraries\DataTable;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;

class ProductController extends Controller
{

    public function __construct()
    {
        parent::__construct('adminData', 'admin');
        $this->breadcrumbTitle = 'Products';
        $this->breadcrumbs[route('admin.home.index')] = ['icon' => 'fa fa-fw fa-home', 'title' => 'Dashboard'];
        $this->breadcrumbs[route('admin.orders.index')] = ['icon' => 'fa fa-fw fa-money', 'title' => 'Manage Products'];
    }

    public function index()
    {
        return view('admin.products.index');
    }

    public function all()
    {
        $columns = [
            ['db' => 'id', 'dt' => 'id'],
            ['db' => 'title', 'dt' => 'title'],
            ['db' => 'slug', 'dt' => 'slug'],
        ];
        DataTable::init(new Product(), $columns);
        $product = DataTable::get();
        if (sizeof($product['data']) > 0) {
            foreach ($product['data'] as $key => $data) {
                $product['data'][$key]['actions'] = '
                            <a class="btn btn-light btn-active-light-primary btn-sm " data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                                Actions
                                <span class="svg-icon svg-icon-5 m-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                            <path d="M6.70710678,15.7071068 C6.31658249,16.0976311 5.68341751,16.0976311 5.29289322,15.7071068 C4.90236893,15.3165825 4.90236893,14.6834175 5.29289322,14.2928932 L11.2928932,8.29289322 C11.6714722,7.91431428 12.2810586,7.90106866 12.6757246,8.26284586 L18.6757246,13.7628459 C19.0828436,14.1360383 19.1103465,14.7686056 18.7371541,15.1757246 C18.3639617,15.5828436 17.7313944,15.6103465 17.3242754,15.2371541 L12.0300757,10.3841378 L6.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 11.999999) rotate(-180.000000) translate(-12.000003, -11.999999)"></path>
                                        </g>
                                    </svg>
                                </span>
                            </a>
                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
                                <div class="menu-item px-3">
                                    <a href="' . route('admin.products.edit', $data['id']) . '" class="menu-link px-3" data-kt-docs-table-filter="edit_row">
                                        Edit
                                    </a>
                                </div>
                                <div class="menu-item px-3">
                                    <a href="javascript:{};" data-url="' . route('admin.products.destroy', $data['id']) . '" class="menu-link px-3" data-kt-docs-table-filter="delete_row">
                                        Delete
                                    </a>
                                </div>
                            </div>
                        ';
            }
        }
        $product['recordsFiltered'] = $product['meta']['total'];
        $product['recordsTotal'] = $product['meta']['total'];
        return response($product);
    }

    public function edit($id)
    {
        $heading = (($id > 0) ? 'Edit Order' : 'Add Order');
        $this->breadcrumbs['javascript:{};'] = ['icon' => 'fa fa-fw fa-money', 'title' => $heading];
        $categories = Category::select(['id', 'title'])->get();
        $attributes = Attribute::where('parent_id', null)->select(['id', 'title', 'parent_id'])->whereHas('subAttributes')->with(['subAttributes:id,title,parent_id'])->get();
        return view('admin.products.edit', [
            'productId' => $id,
            'categories' => $categories,
            'attributes' => $attributes
        ]);

    }

    public function destroy($id)
    {
        try {
            Product::findOrFail($id)->delete();
            return response(['msg' => 'Product deleted Successfully']);
        }
        catch (\Exception $e) {
            return response(['err' => 'Unable to delete'],400);
        }
    }


}
