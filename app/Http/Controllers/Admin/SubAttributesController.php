<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeRequest;
use App\Models\Attribute;

class SubAttributesController extends Controller
{
    public function __construct()
    {
        parent::__construct('adminData', 'admin');
        $this->breadcrumbTitle = 'Sub Attributes';
        $this->breadcrumbs[route('admin.home.index')] = ['icon' => 'fa fa-fw fa-home', 'title' => 'Dashboard'];
    }

    public function getViewParams($id = 0)
    {
        $attribute = new Attribute();
        if ($id > 0) {
            $attribute = Attribute::findOrFail($id);
        }
        return $attribute;
    }

    public function edit($parentId, $id)
    {
        $heading = (($id > 0) ? 'Edit Sub Attribute' : 'Add Sub Attribute');
        $this->breadcrumbs[route('admin.attributes.index')] = ['icon' => 'fa fa-fw fa-building', 'title' => 'Manage Sub Attributes'];
        $this->breadcrumbs['javascript:{};'] = ['icon' => 'fa fa-fw fa-money', 'title' => $heading];
        return view('admin.attributes.edit', [
            'method' => 'PUT',
            'attributeId' => $id,
            'parent' => $parentId,
            'attribute' => $this->getViewParams($id),
            'action' => route('admin.attributes.sub-attributes.update',[$parentId,$id])
        ]);
    }

    public function update(AttributeRequest $request, $parentId, $id)
    {
        try {
            $data = $request->only(['title']);
            $data['parent_id'] = $parentId;
            Attribute::updateOrCreate(['id'=> $id], $data);
            return redirect(route('admin.attributes.index'))->with('status', 'Operation Successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->withInput()->with('err', $e->getMessage());
        }
    }

}

