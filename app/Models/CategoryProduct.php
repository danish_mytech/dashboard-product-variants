<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;

    protected $fillable = [
        'category_id','product_id'
    ];
    protected $casts = [
        'category_id' => 'int',
        'product_id' => 'int',
    ];

}
