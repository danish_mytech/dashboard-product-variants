<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $dateFormat = 'U';
    public static $snakeAttributes = false;
    protected $fillable = [
        'parent_id',
        'title',
    ];
    protected $casts = [
        'created_at' => 'int',
        'updated_at' => 'int',
        'deleted_at' => 'int',
    ];
    protected $hidden = ['pivot'];


    public function subAttributes()
    {
        return $this->hasMany(static::class, 'parent_id');
    }
}
