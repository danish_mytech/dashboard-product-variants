<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AttributeProduct extends Model
{
    public static $snakeAttributes = false;

    protected $fillable = [
        'attribute_id','product_id', 'sub_attribute_id'
    ];
    protected $casts = [
        'attribute_id' => 'int',
        'product_id' => 'int',
        'sub_attribute_id' => 'int',
    ];

}
