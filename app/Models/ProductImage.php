<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;

    protected $fillable = [
        'image','product_id', 'is_default'
    ];
    protected $casts = [
        'created_at' => 'int',
        'updated_at' => 'int',
        'deleted_at' => 'int',
    ];

}
