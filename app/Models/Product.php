<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Product extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;

    protected $fillable = [
        'slug','title', 'detail', 'price', 'quantity'
    ];
    protected $casts = [
        'created_at' => 'int',
        'updated_at' => 'int',
        'deleted_at' => 'int',
    ];

    public function categories(){
        return $this->belongsToMany(Category::class, CategoryProduct::class);
    }

    public function productCategoryIds(){
        return $this->hasMany(CategoryProduct::class);
    }

    public function image(){
        return $this->hasOne(ProductImage::class)->where('is_default', 1);
    }

    public function images(){
        return $this->hasMany(ProductImage::class)->orderBy('is_default', 'desc');
    }

    public function attributes(){
        return $this->belongsToMany(Attribute::class, AttributeProduct::class);
    }

    public function productAttributesIds(){
        return $this->hasMany(AttributeProduct::class);
    }

    /**
     * Boot the model.
     */
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->slug = $model->createSlug($model->title);
            return true;
        });
    }

    private function createSlug($title){
        $slug = \Str::slug($title);
        $count = static::where('slug', 'LIKE', $slug.'%')->count();
        if($count > 0){
            $count++;
            return  $slug.'-'.$count;
        }
        return $slug;
    }

}
