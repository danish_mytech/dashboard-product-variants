<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;

    protected $fillable = [
        'customer_name','customer_family_name', 'phone', 'address'
    ];
    protected $casts = [
        'created_at' => 'int',
        'updated_at' => 'int',
        'deleted_at' => 'int',
    ];

}
