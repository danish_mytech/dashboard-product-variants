<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $dateFormat = 'U';
    public static $snakeAttributes = false;

    protected $fillable = [
        'slug','title'
    ];
    protected $casts = [
        'created_at' => 'int',
        'updated_at' => 'int',
        'deleted_at' => 'int',
    ];

    /**
     * Boot the model.
     */
    protected static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            $model->slug = $model->createSlug($model->title);
            return true;
        });
    }

    private function createSlug($title){

        $slug = \Str::slug($title);
        $count = static::where('slug', 'LIKE', $slug.'%')->count();
        if($count > 0){
            $count++;
            return  $slug.'-'.$count;
        }
        return $slug;
    }

}
