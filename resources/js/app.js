require('./bootstrap');
import Vue from 'vue/dist/vue';

window.Vue = new Vue;
Vue.component('image-uploader', require('./common/image-uploader.vue').default);
Vue.component('product-form', require('./components/product-form.vue').default);

const app = new Vue({
    el: '#main-app',
});

