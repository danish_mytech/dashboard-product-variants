@extends('admin.layouts.app')

@section('content')
    <div class="container" id="kt_docs_content_container">
        <!--begin::Card-->
        <div class="card card-docs mb-2">
            <div class="card-body text-gray-700">
                <!--begin::Block-->
                <div class="py-5">
                    {!! Form::model($order, ['url' => $action, 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                    <input type="hidden" value="PUT" name="_method">
                    <div class="rounded border p-10">
                        <div class="mb-10">
                            <label class="form-label"> {!! __('Customer Name') !!}</label>
                            <input type="text" class="form-control" name="customer_name"
                                   value="{{old('name', $order->customer_name)}}" placeholder="Customer Name" required>
                            @if ($errors->has('customer_name'))
                                <div class="form-control-feedback text-danger">{{ $errors->first('customer_name') }}</div>
                            @endif
                        </div>
                        <div class="mb-10">
                            <label class="form-label"> {!! __('Customer Family Name') !!}</label>
                            <input type="text" class="form-control" name="customer_family_name"
                                   value="{{old('name', $order->customer_family_name)}}" placeholder="Customer Family Name" required>
                            @if ($errors->has('customer_family_name'))
                                <div class="form-control-feedback text-danger">{{ $errors->first('customer_family_name') }}</div>
                            @endif
                        </div>
                        <div class="mb-10">
                            <label class="form-label"> {!! __('Phone') !!}</label>
                            <input type="text" class="form-control" name="phone"
                                   value="{{old('name', $order->phone)}}" placeholder="Phone" required>
                            @if ($errors->has('phone'))
                                <div class="form-control-feedback text-danger">{{ $errors->first('phone') }}</div>
                            @endif
                        </div>
                        <div class="mb-10">
                            <label class="form-label"> {!! __('Address') !!}</label>
                            <input type="text" class="form-control" name="address"
                                   value="{{old('name', $order->address)}}" placeholder="Address" required>
                            @if ($errors->has('address'))
                                <div class="form-control-feedback text-danger">{{ $errors->first('address') }}</div>
                            @endif
                        </div>
                        <a href="{!! route('admin.orders.index') !!}" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-dark">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Block-->
            </div>
        </div>
    </div>
@endsection

