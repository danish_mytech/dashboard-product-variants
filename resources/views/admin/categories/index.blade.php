@extends('admin.layouts.app')


@push('stylesheet-page-level')
    <link rel="stylesheet" href="{{asset('assets/admin/css/datatables.bundle.css')}}">
@endpush

@push('script-page-level')
    <script src="{{asset('assets/admin/js/datatables.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/admin/js/datatables/csrf_token.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/admin/js/datatables/categories.js')}}" type="text/javascript"></script>

@endpush

@section('content')
    <div class="container" id="kt_docs_content_container">
        <!--begin::Card-->
        <div class="card card-docs mb-2">
            <!--begin::Card Body-->
            <div class="card-body text-gray-700">
                <!--begin::Section-->
                <div class="py-5">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-stack justify-content-end mb-5">
                        <!--begin::Toolbar-->
                        <div class="d-flex justify-content-end" data-kt-docs-table-toolbar="base">
                            <!--begin::Add customer-->
                            <a href="{!! route('admin.categories.edit', 0) !!}" type="button" class="btn btn-primary">
                                Add Category
                            </a>
                            <!--end::Add customer-->
                        </div>
                        <!--end::Toolbar-->
                    </div>

                    <!--begin::Datatable-->
                    <table id="kt_datatable_example_1" class="table align-middle table-row-dashed fs-6 gy-5">
                        <thead>
                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                            <th>Id</th>
                            <th>Slug</th>
                            <th>Title</th>
                            <th class="text-end min-w-100px">Actions</th>
                        </tr>
                        </thead>
                        <tbody class="text-gray-600 fw-bold">
                        </tbody>
                    </table>
                    <!--end::Datatable-->

                    <!--end::Wrapper-->
                </div>
            </div>
        </div>
    </div>

@endsection
