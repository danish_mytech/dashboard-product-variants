<div class="d-flex align-items-center" id="kt_header_nav">
    <!--begin::Page title-->
    <div data-kt-swapper="true" data-kt-swapper-mode="prepend"  class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
        <!--begin::Title-->
        <h1 class="d-flex align-items-center text-dark fw-bolder fs-3 my-1">{!! $breadcrumbTitle !!}</h1>
        <!--end::Title-->
        @if(isset($breadcrumbs) && count($breadcrumbs) > 0)
        <!--begin::Separator-->
        <span class="h-20px border-gray-200 border-start mx-4"></span>
        <!--end::Separator-->
        <!--begin::Breadcrumb-->
        <ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">

            <!--begin::Item-->
            @foreach($breadcrumbs as $url => $bc)

            @if(!$loop->last)
                    <li class="breadcrumb-item text-muted">
                        <a href="{!! $url !!}" class="text-muted text-hover-primary">{!! $bc['title'] !!}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <span class="bullet bg-gray-200 w-5px h-2px"></span>
                    </li>
            @else
                    <li class="breadcrumb-item text-dark">{!! $bc['title'] !!}</li>
            @endif
            @endforeach
            <!--end::Item-->
        </ul>
        <!--end::Breadcrumb-->
        @endif
    </div>
    <!--end::Page title-->
</div>
