@extends('admin.layouts.login')

@section('content')

    <!--begin::Wrapper-->
    <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
        <!--begin::Form-->
        <form class="form w-100" novalidate="novalidate" id="kt_sign_in_form" action="{!! route('admin.auth.login.login') !!}" METHOD="post">
            {{ csrf_field() }}
            <!--begin::Heading-->
            <div class="text-center mb-10">
                <!--begin::Title-->
                <h1 class="text-dark mb-3">Sign In</h1>
                <!--end::Title-->
            </div>
            <!--begin::Heading-->
            <!--begin::Input group-->
            <div class="fv-row mb-10 {{ $errors->has('email') ? ' has-error' : '' }}">
                <!--begin::Label-->
                <label class="form-label fs-6 fw-bolder text-dark">Email</label>
                <!--end::Label-->
                <!--begin::Input-->
                <input class="form-control form-control-lg form-control-solid" value="{{old('email')}}"  type="text" placeholder="Email" name="email" autocomplete="off" required />
                <!--end::Input-->
                @if ($errors->has('email'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('email') }}</div>
                @endif
            </div>
            <!--end::Input group-->
            <!--begin::Input group-->
            <div class="fv-row mb-10 {{ $errors->has('password') ? ' has-error' : '' }}">
                <!--begin::Wrapper-->
                <div class="d-flex flex-stack mb-2">
                    <!--begin::Label-->
                    <label class="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
                    <!--end::Label-->
                </div>
                <!--end::Wrapper-->
                <!--begin::Input-->
                <input class="form-control form-control-lg form-control-solid" type="password" placeholder="Password" name="password" required />
                <!--end::Input-->
                @if ($errors->has('password'))
                    <div class="form-control-feedback text-danger">{{ $errors->first('password') }}</div>
                @endif
            </div>
            <!--end::Input group-->
            <!--begin::Actions-->
            <div class="text-center">
                <!--begin::Submit button-->
                <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
                    <span class="indicator-label">Login</span>
                </button>
                <!--end::Submit button-->

            </div>
            <!--end::Actions-->
        </form>
        <!--end::Form-->
    </div>
    <!--end::Wrapper-->


{{--    <div class="m-login__signin">--}}
{{--        <div class="m-login__head">--}}
{{--            <h3 class="m-login__title">--}}
{{--                {{config('settings.sign_in_text_login_page')}}--}}
{{--            </h3>--}}
{{--        </div>--}}
{{--        <form class="m-login__form m-form" action="{!! route('admin.auth.login.login') !!}" METHOD="post">--}}
{{--            {{ csrf_field() }}--}}
{{--            <div class="form-group m-form__group {{ $errors->has('email') ? ' has-error' : '' }}">--}}
{{--                <input class="form-control m-input"  value="{{old('email')}}"  type="text" placeholder="Email" name="email" autocomplete="off" required>--}}
{{--            </div>--}}
{{--            @if ($errors->has('email'))--}}
{{--                <div class="form-control-feedback">{{ $errors->first('email') }}</div>--}}
{{--            @endif--}}
{{--            <div class="form-group m-form__group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
{{--                <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password" required>--}}
{{--            </div>--}}
{{--            <div class="row m-login__form-sub">--}}
{{--                <div class="col m--align-left m-login__form-left">--}}
{{--                    <label class="m-checkbox  m-checkbox--light">--}}
{{--                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>--}}
{{--                        Remember me--}}
{{--                        <span></span>--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="m-login__form-action">--}}
{{--                <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">--}}
{{--                    Sign In--}}
{{--                </button>--}}
{{--            </div>--}}
{{--        </form>--}}
{{--    </div>--}}
@endsection
