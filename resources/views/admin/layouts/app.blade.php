<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
    <head>
        <meta charset="utf-8" />
        <title>
            Admin Dashboard
        </title>
        <meta name="description" content="Bootstrap alert examples">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

        <script>
            WebFont.load({
                active: function() {
                    sessionStorage.fonts = true;
                },
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]}
            });
        </script>
        <link href="{{asset('assets/admin/css/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/admin/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        @stack('stylesheet-page-level')
        @stack('stylesheet-end')
        <script type="text/javascript">
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
                'baseUrl' => url('/')."/admin/",
                'apiUrl' => url('/')."/api/",
                'base' => url('/').'/',
                'token' => ($user) ? $user['token']: ''
            ]) !!};
        </script>
    </head>
    <body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px" >
    <div class="d-flex flex-column flex-root" id="main-app">
        <!--begin::Page-->
        <div class="page d-flex flex-row flex-column-fluid">
            @include('admin.common.left-sidebar')
            <!--begin::Wrapper-->
                <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
                    @include('admin.common.header')
                    <div class=" d-flex flex-column flex-column-fluid" id="kt_content">
                        @yield('content')
                    </div>
                    <!--begin::Footer-->
                    @include('admin.common.footer')
                </div>
        </div>
    </div>

{{--    <div class="m-grid m-grid--hor m-grid--root m-page">--}}
{{--            @include('admin.common.header')--}}
{{--            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">--}}
{{--                @include('admin.common.left-sidebar')--}}
{{--                <div class="m-grid__item m-grid__item--fluid m-wrapper">--}}
{{--                    @yield('breadcrumb')--}}
{{--                    <div class="m-content">--}}
{{--                        @include('admin.common.alerts')--}}
{{--                        @yield('content')--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            @include('admin.common.footer')--}}
{{--        </div>--}}
{{--        <div class="m-scroll-top m-scroll-top--skin-top" data-toggle="m-scroll-top" data-scroll-offset="500" data-scroll-speed="300">--}}
{{--            <i class="la la-arrow-up"></i>--}}
{{--        </div>--}}

        <script src="{{asset('assets/admin/js/plugins.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('assets/admin/js/scripts.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/app.js')}}" type="text/javascript"></script>
        <!-- end of global js -->
        @stack('script-page-level')
        <!-- custom scripts -->
        @stack('script-end')




        @yield('custom_js')
    </body>
</html>
