<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8" />
    <title>
        {!! config('settings.company_name') !!}
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>

    <script>
        WebFont.load({
            active: function() {
                sessionStorage.fonts = true;
            },
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]}
        });
    </script>
    <link href="{{asset('assets/admin/css/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/admin/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    @stack('stylesheet-page-level')
</head>

<body id="kt_body" class="bg-body"  >

<div class="d-flex flex-column flex-root">
    <!--begin::Authentication - Sign-in -->
    <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style="background-image: url('{{asset('assets/admin/img/14.png')}}'">
        <!--begin::Content-->
        <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
            <!--begin::Logo-->
            <a class="mb-12">
                <img alt="Logo" src="{{asset('assets/admin/img/logo-1.svg')}}" class="h-40px" />
            </a>
            <!--end::Logo-->
            @include('admin.common.alerts')
            @yield('content')
        </div>
    </div>
</div>



<script src="{{asset('assets/admin/js/plugins.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/admin/js/scripts.bundle.js')}}" type="text/javascript"></script>
</body>
</html>
