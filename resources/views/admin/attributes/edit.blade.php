@extends('admin.layouts.app')

@section('content')
    <div class="container" id="kt_docs_content_container">
        <!--begin::Card-->
        <div class="card card-docs mb-2">
            <div class="card-body text-gray-700">
                <!--begin::Block-->
                <div class="py-5">
                    {!! Form::model($attribute, ['url' => $action, 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}
                    <input type="hidden" value="PUT" name="_method">
                    <div class="rounded border p-10">
                        <div class="mb-10">
                            <label class="form-label"> {!! __('Enter Title') !!}</label>
                            <input type="text" class="form-control" name="title"
                                   value="{{old('name', $attribute->title)}}" placeholder="Title" required>
                            @if ($errors->has('title'))
                                <div class="form-control-feedback text-danger">{{ $errors->first('title') }}</div>
                            @endif
                        </div>
                        <a href="{!! route('admin.attributes.index') !!}" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-dark">Submit</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--end::Block-->
            </div>
        </div>
    </div>
@endsection

