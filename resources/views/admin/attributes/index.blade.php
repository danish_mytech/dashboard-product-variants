@extends('admin.layouts.app')



@push('stylesheet-page-level')
<style>
    .tree-category-list-mt button{
        background: none;
        border: none;
        cursor: pointer;
    }
    .tree-category-list-mt i{
        color: #222222;
    }
    .tree-category-list-mt .tree,
    .tree ul {
        margin:0 0 0 1em; /* indentation */
        padding:0;
        list-style:none;
        color:#222222;
        position:relative;
    }

    .tree-category-list-mt .tree ul {margin-left:.5em} /* (indentation/2) */

    .tree-category-list-mt .tree:before,
    .tree ul:before {
        content:"";
        display:block;
        width:0;
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        border-left:1px solid;
    }

    .tree-category-list-mt .tree li {
        margin:0;
        padding:0 1.5em; /* indentation + .5em */
        line-height:2em; /* default list item's `line-height` */
        font-weight:bold;
        position:relative;
    }

    .tree-category-list-mt .tree li:before {
        content:"";
        display:block;
        width:10px; /* same with indentation */
        height:0;
        border-top:1px solid;
        margin-top:-1px; /* border top width */
        position:absolute;
        top:1em; /* (line-height/2) */
        left:0;
    }

    .tree-category-list-mt .tree li:last-child:before {
        background:white; /* same with body background */
        height:auto;
        top:1.5em; /* (line-height/2) */
        bottom:0;
    }

    .subattributes-li{
        padding-top: 5px !important;
        padding-bottom: 5px !important;
    }

    .btn.btn-icon{
        padding: 10px;
        height: auto;
        width: auto;
        margin-right: 5px;
    }

</style>

@endpush



@push('script-page-level')

    <script>

        $(document).ready(function () {

            $('.delete-record-button').on('click', function (e) {

                var url = $(this).data('url');

                console.log(url);

                Swal.fire({

                        title: "Are You Sure You Want To Delete This?",

                        text: "Deleting this will effect all the products that are using this attribute, they will stop showing to the users.",

                        type: "warning",

                        showCancelButton: true,

                        confirmButtonColor: "#DD6B55",

                        confirmButtonText: "Delete",

                        cancelButtonText: "No",

                        closeOnConfirm: false,

                        closeOnCancel: false,

                        showLoaderOnConfirm: true

                    },

                   ).then( function (result) {
                    console.log(result)
                    if (result.value) {

                        $.ajax({

                            type: 'delete',

                            url: url,

                            dataType: 'json',

                            headers: {

                                'X-CSRF-TOKEN': window.Laravel.csrfToken

                            }

                        })

                            .done(function (res) {
                                toastr.success("Attribute Deleted Successfully!");
                                location.reload();
                            })

                            .fail(function (res) {
                                toastr.success("You have deleted inquiry successfully!");
                            });

                    } else {

                        swal.close();

                    }

                });

            });

        })

    </script>



@endpush



@section('content')

    <div class="container" id="kt_docs_content_container">
        <!--begin::Card-->
        <div class="card card-docs mb-2 tree-category-list-mt">
            <!--begin::Card Body-->
            <div class="card-body  text-gray-700">
                <!--begin::Section-->
                <div class="py-5">
                    <!--begin::Wrapper-->
                    <div class="d-flex flex-stack justify-content-end mb-5">
                        <!--begin::Toolbar-->
                        <div class="d-flex justify-content-end" data-kt-docs-table-toolbar="base">
                            <!--begin::Add customer-->
                            <a href="{!! route('admin.attributes.edit', 0) !!}" type="button" class="btn btn-primary">
                                Add Attribute
                            </a>
                            <!--end::Add customer-->
                        </div>
                        <!--end::Toolbar-->
                    </div>

                    <ul class="tree ng-tns-c8-2 ng-star-inserted">

                        @forelse($attributes as $key => $attribute)

                            <li class="ng-tns-c8-2 ng-star-inserted">{!! $attribute->title !!}

                                <a class="btn btn-icon btn-primary" data-placement="top"

                                   data-toggle="tooltip" title="Add sub-Attribute"

                                   href="{!! route('admin.attributes.sub-attributes.edit', [$attribute->id,0]) !!}"><i

                                        class="fa fa-plus text-white" aria-hidden="true"></i></a><a

                                    class="btn btn-icon btn-success" data-placement="top" data-toggle="tooltip"

                                    title="Edit" href="{!! route('admin.attributes.edit', $attribute->id) !!}"><i

                                        class="fa fa-pencil-alt text-white" aria-hidden="true"></i></a>

                                <a href="javascript:{}"
                                   data-url=" {!! route('admin.attributes.destroy', $attribute->id) !!} "
                                   class="btn btn-icon btn-danger delete-record-button"

                                   data-placement="top" data-toggle="tooltip"

                                   style="background-color: #dc3545;border-color: #dc3545;" title="Delete"><i

                                        class="fa fa-trash text-white" aria-hidden="true"></i></a>

                                <ul class="ng-tns-c8-2 ng-star-inserted">

                                    @forelse($attribute->subAttributes as $key2 => $subAttribute)

                                        <li class="ng-tns-c8-2 ng-star-inserted subattributes-li"> {!! $subAttribute->title !!}

                                            <a class="btn btn-icon btn-success" data-placement="top"
                                               data-toggle="tooltip"

                                               title="Edit"
                                               href="{!! route('admin.attributes.sub-attributes.edit', [$attribute->id,$subAttribute->id]) !!}"><i

                                                    class="fa fa-pencil-alt text-white" aria-hidden="true"></i></a>

                                            <a href="javascript:{}"
                                               data-url=" {!! route('admin.attributes.destroy', $subAttribute->id) !!} "
                                               class="btn btn-icon btn-danger  delete-record-button"

                                               data-placement="top" data-toggle="tooltip"

                                               style="background-color: #dc3545;border-color: #dc3545;" title="Delete"><i

                                                    class="fa fa-trash text-white" aria-hidden="true"></i></a>

                                        </li>

                                    @empty



                                    @endforelse

                                </ul>

                            </li>

                        @empty

                        @endforelse


                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection

